﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JackCompiler
{
    #region Enumerations

    static class Kinds
    {
        public const string Field = "field";
        public const string Static = "static";
        public const string Argument = "argument";
        public const string Local = "local";
    }

    #endregion

    class SymbolTable
    {
        readonly Dictionary<string, Info> _symTable;
        readonly Dictionary<string, int> _indexesByKind;

        public SymbolTable()
        {
            _symTable = new Dictionary<string, Info>();
            _indexesByKind = new Dictionary<string, int>();
        }

        public void Add(string symName, string type, string kind)
        {
            int index = 0;
            if (!_indexesByKind.ContainsKey(kind))
                _indexesByKind.Add(kind, index);
            else
                index = ++_indexesByKind[kind];

            var info = new Info(type, kind, index.ToString());
            _symTable.Add(symName, info);
        }

        public Info GetInfo(string symName)
        {
            Info info;
            _symTable.TryGetValue(symName, out info);
            return info;
        }

        public IList<Info> GetInfos(Func<Info, bool> predicate)
        {
            var infos = new List<Info>();

            foreach (string key in _symTable.Keys)
            {
                Info info = _symTable[key];
                if (predicate.Invoke(info))
                    infos.Add(info);
            }

            return infos;
        }

        #region Nested Class: Info

        public class Info
        {
            readonly string _type;
            readonly string _kind;
            readonly string _index;

            public Info(string type, string kind, string index)
            {
                _type = type;
                _kind = kind;
                _index = index;
            }

            public string Index
            {
                get { return _index; }
            }

            public string Kind
            {
                get { return _kind; }
            }

            public string Type
            {
                get { return _type; }
            }
        }

        #endregion
    }
}
