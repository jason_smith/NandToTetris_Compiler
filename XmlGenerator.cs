﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using JackCompiler.Parsing;
using JackCompiler.Tokenizing;

namespace JackCompiler
{
    static class XmlGenerator
    {
        const string Ext = ".xml";
        const string Tokens = "tokens";

        public static void Write(IList<Token> tokens, string inputFile)
        {
            var sb = new StringBuilder();
            sb.AppendLine("<" + Tokens + ">");

            foreach (Token token in tokens)
            {
                sb.Append("<" + token.Type + "> ");
                sb.Append(token.Value);
                sb.AppendLine(" </" + token.Type + ">");
            }

            sb.AppendLine("</" + Tokens + ">");

            string dir = Path.GetDirectoryName(inputFile);
            string fileName = Path.GetFileNameWithoutExtension(inputFile) + "T_js" + Ext;
            string path = Path.Combine(dir, fileName);
            File.WriteAllText(path, sb.ToString());
        }

        public static void Write(Node parseTree, string inputFile)
        {
            var sb = new StringBuilder();

            ProcessNode(parseTree, sb, string.Empty);

            string dir = Path.GetDirectoryName(inputFile);
            string fileName = Path.GetFileNameWithoutExtension(inputFile) + "_js" + Ext;
            string path = Path.Combine(dir, fileName);
            File.WriteAllText(path, sb.ToString());
        }

        private static void ProcessNode(Node node, StringBuilder sb, string indentation)
        {
            sb.Append(indentation);
            sb.AppendLine(node.ToString());

            foreach (Node child in node.Children)
                ProcessNode(child, sb, indentation + "   ");
        }
    }
}
