﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JackCompiler.Tokenizing
{
    static class Tokenizer
    {
        #region Enumerations

        enum CommentType
        {
            None,
            Line,
            Block
        }

        #endregion

        #region Fields

        public readonly static string[] Keywords;
        public readonly static char[] Symbols;
        public const string Bool = "boolean";
        public const string Char = "char";
        public const string Class = "class";
        public const string Ctor = "constructor";
        public const string Do = "do";
        public const string Else = "else";
        public const string False = "false";
        public const string Field = "field";
        public const string Function = "function";
        public const string If = "if";
        public const string Int = "int";
        public const string Let = "let";
        public const string Method = "method";
        public const string Null = "null";
        public const string Return = "return";
        public const string Static = "static";
        public const string This = "this";
        public const string True = "true";
        public const string Var = "var";
        public const string Void = "void";
        public const string While = "while";
        public const char SymLeftCurly = '{';
        public const char SymRightCurly = '}';
        public const char SymLeftParen = '(';
        public const char SymRightParen = ')';
        public const char SymLeftBracket = '[';
        public const char SymRightBracket = ']';
        public const char SymDot = '.';
        public const char SymComma = ',';
        public const char SymSemiColon = ';';
        public const char SymPlus = '+';
        public const char SymMinus = '-';
        public const char SymMult = '*';
        public const char SymDiv = '/';
        public const char SymAnd = '&';
        public const char SymOr = '|';
        public const char SymLT = '<';
        public const char SymGT = '>';
        public const char SymEq = '=';
        public const char SymNot = '~';
        public const char SymQuote = '"';

        #endregion

        #region Constructors

        static Tokenizer()
        {
            Keywords = new string[] { Bool, Char, Class, Ctor, Do, Else, False, Field, Function,
                If, Int, Let, Method, Null, Return, Static, This, True, Var, Void, While };

            Symbols = new char[] { SymLeftCurly, SymRightCurly, SymLeftParen, SymRightParen, SymLeftBracket,
                SymRightBracket, SymDot, SymComma, SymSemiColon, SymPlus, SymMinus,SymMult, SymDiv,
                SymAnd, SymOr, SymLT, SymGT, SymEq, SymNot, SymQuote };
        }

        #endregion

        #region Methods

        private static int GetCommentEndIndex(string text, int startIndex, CommentType type)
        {
            if (type == CommentType.Line)
            {
                int newLineIndex = text.IndexOf('\n', startIndex);
                if (newLineIndex > 0)
                    return newLineIndex;

                return text.Length - 1;
            }

            if (type == CommentType.Block)
                return text.IndexOf("*/", startIndex) + 1;

            return startIndex;
        }

        private static CommentType GetCommentType(char? left, char right)
        {
            if (left != null && left.Value == SymDiv)
            {
                if (right == SymDiv)
                    return CommentType.Line;

                if (right == SymMult)
                    return CommentType.Block;
            }

            return CommentType.None;
        }

        private static int GetStringEndIndex(string text, int startIndex)
        {
            if (startIndex >= text.Length - 1)
                throw new IndexOutOfRangeException("Unable to find end of string constant.");

            int index = text.IndexOf(SymQuote, startIndex + 1);
            if (index < 0)
                throw new InvalidOperationException("Unable to find end of string constant.");

            return index;
        }

        private static Token GetToken(string chunk)
        {
            char c = chunk[0];
            if (char.IsDigit(c))
                return new Token(chunk, TokenTypes.Integer);
            else if (c == SymQuote)
                return new Token(chunk.Substring(1, chunk.Length - 1), TokenTypes.String);
            else if (Keywords.Contains(chunk))
                return new Token(chunk, TokenTypes.Keyword);
            else if (char.IsLetter(c) || c == '_')
                return new Token(chunk, TokenTypes.Identifier);
            else
                throw new InvalidDataException("Invalid token detected: " + chunk);
        }

        public static IList<Token> GetTokens(string file)
        {
            var tokens = new List<Token>();

            char? prevChar = null;
            string chunk = string.Empty;
            string text = File.ReadAllText(file);
            for (int i = 0; i < text.Length; i++)
            {
                char character = text[i];
                
                CommentType commentType = GetCommentType(prevChar, character);
                if (commentType != CommentType.None)
                {
                    if (tokens.Count > 0)
                        tokens.RemoveAt(tokens.Count - 1);

                    if (chunk.Length > 0)
                    {
                        chunk = chunk.Substring(0, chunk.Length - 1);
                        if (!string.IsNullOrEmpty(chunk))
                        {
                            tokens.Add(GetToken(chunk));
                            chunk = string.Empty;
                        }
                    }

                    i = GetCommentEndIndex(text, i, commentType);
                    prevChar = null;
                    continue;
                }
                else if (char.IsWhiteSpace(character))
                {
                    if (!string.IsNullOrEmpty(chunk))
                    {
                        tokens.Add(GetToken(chunk));
                        chunk = string.Empty;
                    }
                }
                else if (Symbols.Contains(character))
                {
                    if (!string.IsNullOrEmpty(chunk))
                        tokens.Add(GetToken(chunk));

                    if (character == SymQuote) // process string
                    {
                        int endIndex = GetStringEndIndex(text, i);
                        tokens.Add(GetToken(text.Substring(i, endIndex - i)));
                        i = endIndex;
                    }
                    else
                        tokens.Add(new Token(character.ToString(), TokenTypes.Symbol));

                    chunk = string.Empty;
                }
                else
                    chunk += character;

                prevChar = character;
            }

            return tokens;
        }

        #endregion
    }
}
