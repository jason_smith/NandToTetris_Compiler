﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JackCompiler.Tokenizing
{
    #region Enumerations

    struct TokenTypes
    {
        public const string Keyword = "keyword";
        public const string Symbol = "symbol";
        public const string Integer = "integerConstant";
        public const string String = "stringConstant";
        public const string Identifier = "identifier";
    }

    #endregion

    [DebuggerDisplay("Type={_type}, Value={_value}")]
    class Token
    {
        readonly string _value;
        readonly string _type;

        public Token(string value, string type)
        {
            _value = value;
            _type = type;
        }
        
        public string Type
        {
            get { return _type; }
        }

        public string Value
        {
            get { return _value; }
        }
    }
}
