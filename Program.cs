﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using JackCompiler.Parsing;
using JackCompiler.Tokenizing;

namespace JackCompiler
{
    class Program
    {
        const string Ext = ".jack";

        private static string[] GetFiles(string[] args)
        {
            if (args.Length == 0)
                throw new InvalidOperationException("Must specify a directory or jack file(s).");

            if (args.Length == 1)
            {
                if (Directory.Exists(args[0]))
                    return Directory.GetFiles(args[0], "*" + Ext, SearchOption.AllDirectories);
                else if (!args[0].EndsWith(Ext, StringComparison.OrdinalIgnoreCase))
                    throw new InvalidOperationException("Invalid directory or jack file: " + args[0]);
            }

            foreach (string arg in args)
            {
                if (!arg.EndsWith(Ext, StringComparison.OrdinalIgnoreCase))
                    throw new InvalidOperationException("Invalid jack files.");
            }

            return args;
        }

        private static void Main(string[] args)
        {
            IList<string> files = GetFiles(args);

            foreach (string file in files)
            {
                IList<Token> tokens = Tokenizer.GetTokens(file);
                //XmlGenerator.Write(tokens, file);

                Class parseTree = Parser.Parse(tokens);
                //XmlGenerator.Write(parseTree, file);

                var compiler = new Compiler(parseTree, file);
                compiler.Compile();
            }
        }
    }
}
