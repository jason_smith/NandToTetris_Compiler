﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JackCompiler
{
    enum VmSegment
    {
        Arg,
        Local,
        Temp,
        Static,
        Pointer,
        Const,
        This,
        That
    }

    class VmWriter
    {
        readonly StringBuilder _sb;

        public VmWriter()
        {
            _sb = new StringBuilder();
        }

        public override string ToString()
        {
            return _sb.ToString();
        }

        private string VmSegmentToString(VmSegment seg)
        {
            switch (seg)
            {
                case VmSegment.Arg: return "argument";
                case VmSegment.Const: return "constant";
                case VmSegment.Local: return "local";
                case VmSegment.Pointer: return "pointer";
                case VmSegment.Static: return "static";
                case VmSegment.Temp: return "temp";
                case VmSegment.That: return "that";
                case VmSegment.This: return "this";
                default: throw new InvalidOperationException("Unknown VM segment.");
            }
        }

        public void WriteArithmetic(string operation)
        {
            _sb.AppendLine(operation);
        }

        public void WriteCall(string name, int nArgs)
        {
            _sb.AppendLine("call " + name + " " + nArgs.ToString());
        }

        public void WriteComment(string comment)
        {
            _sb.AppendLine("// " + comment);
        }

        public void WriteFunction(string name, int nLocals)
        {
            _sb.AppendLine("function " + name + " " + nLocals.ToString());
        }

        public void WriteGoto(string label)
        {
            _sb.AppendLine("goto " + label);
        }

        public void WriteIf(string label)
        {
            _sb.AppendLine("if-goto " + label);
        }

        public void WriteLabel(string label)
        {
            _sb.AppendLine("label " + label);
        }

        public void WritePop(VmSegment segment, string index)
        {
            string segStr = this.VmSegmentToString(segment);
            _sb.AppendLine("pop " + segStr + " " + index);
        }

        public void WritePush(VmSegment segment, string index)
        {
            string segStr = this.VmSegmentToString(segment);
            _sb.AppendLine("push " + segStr + " " + index);
        }

        public void WriteReturn()
        {
            _sb.AppendLine("return");
        }
    }
}
