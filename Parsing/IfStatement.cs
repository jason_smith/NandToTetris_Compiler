﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JackCompiler.Parsing
{
    class IfStatement : Node
    {
        readonly Expression _condition;
        readonly Statements _ifStatements;
        readonly Statements _elseStatements;

        public IfStatement(Expression condition, Statements ifStatements, Statements elseStatements)
        {
            _condition = condition;
            _ifStatements = ifStatements;
            _elseStatements = elseStatements;
        }

        public Expression Condition
        {
            get { return _condition; }
        }

        public Statements ElseStatements
        {
            get { return _elseStatements; }
        }

        public Statements IfStatements
        {
            get { return _ifStatements; }
        }

        public override string ToString()
        {
            string str = "IfStatement: Condition=" + _condition.ToString() + Environment.NewLine;
            str += _ifStatements.ToString();
            str += Environment.NewLine + "else statements: \r\n";
            str += _elseStatements.ToString();

            return str;
        }
    }
}
