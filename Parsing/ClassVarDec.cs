﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using JackCompiler.Tokenizing;

namespace JackCompiler.Parsing
{
    class ClassVarDec : Node
    {
        readonly Token _staticOrFieldToken;
        readonly Token _typeToken;
        readonly Token _varNameToken;

        public ClassVarDec(Token staticOrFieldToken, Token typeToken, Token varNameToken)
        {
            _staticOrFieldToken = staticOrFieldToken;
            _typeToken = typeToken;
            _varNameToken = varNameToken;
        }

        public bool IsField
        {
            get { return _staticOrFieldToken.Value == Tokenizer.Field; }
        }

        public bool IsStatic
        {
            get { return _staticOrFieldToken.Value == Tokenizer.Static; }
        }

        public string TypeName
        {
            get { return _typeToken.Value; }
        }

        public string VarName
        {
            get { return _varNameToken.Value; }
        }

        public override string ToString()
        {
            return string.Format("ClassVarDec: {0} {1} {2}", _staticOrFieldToken.Value, _typeToken.Value, _varNameToken.Value); 
        }
    }
}
