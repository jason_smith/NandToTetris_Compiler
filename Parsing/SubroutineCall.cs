﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using JackCompiler.Tokenizing;

namespace JackCompiler.Parsing
{
    class SubroutineCall : Node
    {
        readonly IList<Expression> _expList;
        readonly Token _subroutineName;
        readonly Token _varName;

        public SubroutineCall(Token subroutineName, IList<Expression> expList)
        {
            _subroutineName = subroutineName;
            _expList = expList;
        }

        public SubroutineCall(Token varName, Token subroutineName, IList<Expression> expList)
        {
            _varName = varName;
            _subroutineName = subroutineName;
            _expList = expList;
        }

        public IList<Expression> ParamList
        {
            get { return _expList; }
        }

        public string SubroutineName
        {
            get { return _subroutineName.Value; }
        }

        public string VariableName
        {
            get { return (_varName != null) ? _varName.Value : null; }
        }

        public override string ToString()
        {
            string str = "SubroutineCall: ";

            if (_varName != null)
                str += _varName.Value + ".";

            str += _subroutineName.Value + "(";

            for (int i = 0; i < _expList.Count; i++)
            {
                str += _expList[i].ToString();
                if (i < _expList.Count - 1)
                    str += ", ";
            }

            str += ")";

            return str;
        }
    }
}
