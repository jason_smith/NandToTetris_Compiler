﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using JackCompiler.Tokenizing;

namespace JackCompiler.Parsing
{
    class SubroutineVarDec : Node
    {
        readonly Token _type;
        readonly Token _name;

        public SubroutineVarDec(Token type, Token name)
        {
            _type = type;
            _name = name;
        }

        public string Name
        {
            get { return _name.Value; }
        }

        public string Type
        {
            get { return _type.Value; }
        }

        public override string ToString()
        {
            return "SubroutineVarDec: " + _type.Value + " " + _name.Value;
        }
    }
}
