﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using JackCompiler.Tokenizing;

namespace JackCompiler.Parsing
{
    class LetStatement : Node
    {
        readonly Token _varName;
        readonly Expression _assignment;
        readonly Expression _indexer;
        
        public LetStatement(Token varName, Expression assignment, Expression indexer = null)
        {
            _varName = varName;
            _assignment = assignment;
            _indexer = indexer;
        }

        public Expression Assignment
        {
            get { return _assignment; }
        }

        public Expression Indexer
        {
            get { return _indexer; }
        }

        public string VarName
        {
            get { return _varName.Value; }
        }

        public override string ToString()
        {
            string str = "LetStatement: " + _varName.Value;

            if (_indexer != null)
                str += "[" + _indexer.ToString() + "]";

            str += " = " + _assignment.ToString();
            return str;
        }
    }
}
