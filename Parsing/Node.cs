﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JackCompiler.Parsing
{
    abstract class Node
    {
        readonly IList<Node> _children;

        protected Node()
        {
            _children = new List<Node>();
        }

        public IList<Node> Children
        {
            get { return _children; }
        }

        //public abstract string GetEndXml();

        //public abstract string GetStartXml();
    }
}
