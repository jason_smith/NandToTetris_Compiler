﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using JackCompiler.Tokenizing;

namespace JackCompiler.Parsing
{
    class Expression : Node
    {
        readonly Terminal _term1;
        readonly Terminal _term2;
        readonly Token _op;

        public Expression(Terminal term1, Token op = null, Terminal term2 = null)
        {
            _term1 = term1;
            _op = op;
            _term2 = term2;
        }

        public Token Operator
        {
            get { return _op; }
        }

        public Terminal Term1
        {
            get { return _term1; }
        }

        public Terminal Term2
        {
            get { return _term2; }
        }

        public override string ToString()
        {
            string str = "Expression: " + _term1.ToString();

            if (_op != null)
                str += " " + _op.Value + " " + _term2.ToString();

            return str;
        }
    }
}
