﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JackCompiler.Parsing
{
    class Subroutine : Node
    {
        readonly SubroutineDeclaration _declaration;

        public Subroutine(SubroutineDeclaration declaration)
        {
            _declaration = declaration;
        }

        public SubroutineDeclaration Declaration
        {
            get { return _declaration; }
        }

        public override string ToString()
        {
            return "Subroutine: " + _declaration.ToString();
        }
    }
}
