﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JackCompiler.Parsing
{
    class ReturnStatement : Node
    {
        readonly Expression _retExpression;

        public ReturnStatement(Expression retExpression = null)
        {
            _retExpression = retExpression;
        }

        public Expression ReturnExpression
        {
            get { return _retExpression; }
        }

        public override string ToString()
        {
            string str = "ReturnStatement";
            if (_retExpression != null)
                str += ": " + _retExpression.ToString();

            return str;
        }
    }
}
