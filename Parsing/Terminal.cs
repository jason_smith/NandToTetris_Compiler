﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using JackCompiler.Tokenizing;

namespace JackCompiler.Parsing
{
    class Terminal : Node
    {
        #region Fields

        readonly Terminal _term;
        readonly Expression _exp;
        readonly SubroutineCall _subCall;
        readonly Token _token;
        readonly bool _isArray;

        #endregion

        #region Constructors

        public Terminal(Token term)
        {
            _token = term;
        }

        public Terminal(Token array, Expression expression)
        {
            _token = array;
            _exp = expression;
            _isArray = true;
        }

        public Terminal(SubroutineCall subCall)
        {
            _subCall = subCall;
        }

        public Terminal(Expression expression)
        {
            _exp = expression;
        }

        public Terminal(Token unaryOp, Terminal term)
        {
            _token = unaryOp;
            _term = term;
        }

        #endregion

        #region Properties

        public Expression Expression
        {
            get { return _exp; }
        }

        public bool IsArray
        {
            get { return _isArray; }
        }

        public bool IsConstValue
        {
            get 
            { 
                return _exp == null && 
                    _term == null && 
                    _token != null && 
                    (_token.Type == TokenTypes.Integer || 
                    _token.Type == TokenTypes.String); 
            }
        }

        public bool IsExpression
        {
            get { return !_isArray && _exp != null; }
        }

        public bool IsKeywordConstant
        {
            get { return _token != null && Parser.IsKeywordConstant(_token); }
        }

        public bool IsSubroutineCall
        {
            get { return _subCall != null; }
        }

        public bool IsThis
        {
            get { return _token != null && _token.Type == TokenTypes.Keyword && _token.Value == Tokenizer.This; }
        }

        public bool IsUnaryOpTerm
        {
            get { return _term != null; }
        }

        public bool IsVarName
        {
            get { return _exp == null && _term == null && _token != null && _token.Type == TokenTypes.Identifier; }
        }

        public SubroutineCall SubroutineCall
        {
            get { return _subCall; }
        }

        public Terminal Term
        {
            get { return _term; }
        }

        public Token Token
        {
            get { return _token; }
        }

        #endregion

        #region Methods

        public override string ToString()
        {
            string str = "Term: ";
            if (this.IsArray)
                str += "Array: " + _token.Value + "[" + _exp.ToString() + "]";
            else if (this.IsVarName)
                str += "VarName: " + _token.Value;
            else if (this.IsConstValue)
                str += "Const: " + _token.Value;
            else if (this.IsExpression)
                str += _exp.ToString();
            else if (this.IsSubroutineCall)
                str += _subCall.ToString();
            else if (this.IsUnaryOpTerm)
                str += "UnaryOp: " + _token.Value + _term.ToString();
            else
                str += "UNKNOWN";

            return str;
        }

        #endregion
    }
}
