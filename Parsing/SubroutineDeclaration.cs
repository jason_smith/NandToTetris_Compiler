﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using JackCompiler.Tokenizing;

namespace JackCompiler.Parsing
{
    class SubroutineDeclaration : Node
    {
        // key: paramType, value: paramName
        readonly Dictionary<Token, Token> _paramList;
        readonly Token _routineType;
        readonly Token _returnType;
        readonly Token _name;

        public SubroutineDeclaration(
            Token routineType, 
            Token returnType, 
            Token name, 
            Dictionary<Token, Token> paramList)
        {
            _routineType = routineType;
            _returnType = returnType;
            _name = name;
            _paramList = paramList;
        }

        public bool IsCtor
        {
            get { return _routineType.Value == Tokenizer.Ctor; }
        }

        public bool IsFunction
        {
            get { return _routineType.Value == Tokenizer.Function; }
        }

        public bool IsMethod
        {
            get { return _routineType.Value == Tokenizer.Method; }
        }

        public string Name
        {
            get { return _name.Value; }
        }

        public int ParamCount
        {
            get { return _paramList.Count; }
        }

        public Dictionary<Token, Token> ParamList
        {
            get { return _paramList; }
        }

        public string ReturnType
        {
            get { return _returnType.Value; }
        }

        public override string ToString()
        {
            string str = string.Format("SubroutineDec: {0} {1} {2}(", _routineType.Value, _returnType.Value, _name.Value);

            int count = 1;
            foreach (Token paramType in _paramList.Keys)
            {
                str += string.Format("{0} {1}", paramType.Value, _paramList[paramType].Value);
                if (count < _paramList.Count)
                    str += ", ";
            }

            str += ")";
            return str;
        }
    }
}
