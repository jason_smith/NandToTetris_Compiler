﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JackCompiler.Parsing
{
    class WhileStatement : Node
    {
        readonly Expression _condition;

        public WhileStatement(Expression condition, Statements statements)
        {
            _condition = condition;

            if (statements != null)
                this.Children.Add(statements);
        }

        public Expression Condition
        {
            get { return _condition; }
        }

        public override string ToString()
        {
            return "WhileStatement: Condition=" + _condition.ToString();
        }
    }
}
