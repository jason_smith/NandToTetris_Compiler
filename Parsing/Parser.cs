﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using JackCompiler.Tokenizing;

namespace JackCompiler.Parsing
{
    static class Parser
    {
        private static bool IsBuiltinType(Token token)
        {
            return (token.Type == TokenTypes.Keyword &&
                (token.Value == Tokenizer.Int ||
                token.Value == Tokenizer.Char ||
                token.Value == Tokenizer.Bool));
        }


        private static bool IsComma(Token token)
        {
            return token.Type == TokenTypes.Symbol && token.Value == Tokenizer.SymComma.ToString();
        }


        private static bool IsConstantValue(Token token)
        {
            return token.Type == TokenTypes.Integer || token.Type == TokenTypes.String;
        }


        private static bool IsDot(Token token)
        {
            return token.Type == TokenTypes.Symbol && token.Value == Tokenizer.SymDot.ToString();
        }


        private static bool IsElse(Token token)
        {
            return token.Type == TokenTypes.Keyword && token.Value == Tokenizer.Else;
        }


        public static bool IsKeywordConstant(Token token)
        {
            return token.Type == TokenTypes.Keyword &&
                (token.Value == Tokenizer.True ||
                token.Value == Tokenizer.False ||
                token.Value == Tokenizer.Null ||
                token.Value == Tokenizer.This);
        }


        private static bool IsLeftBracket(Token token)
        {
            return token.Type == TokenTypes.Symbol && token.Value == Tokenizer.SymLeftBracket.ToString();
        }


        private static bool IsLeftCurly(Token token)
        {
            return token.Type == TokenTypes.Symbol && token.Value == Tokenizer.SymLeftCurly.ToString();
        }


        private static bool IsLeftParen(Token token)
        {
            return token.Type == TokenTypes.Symbol && token.Value == Tokenizer.SymLeftParen.ToString();
        }


        private static bool IsOp(Token token)
        {
            return token.Type == TokenTypes.Symbol &&
                (token.Value == Tokenizer.SymPlus.ToString() ||
                token.Value == Tokenizer.SymMinus.ToString() ||
                token.Value == Tokenizer.SymMult.ToString() ||
                token.Value == Tokenizer.SymDiv.ToString() ||
                token.Value == Tokenizer.SymAnd.ToString() ||
                token.Value == Tokenizer.SymOr.ToString() ||
                token.Value == Tokenizer.SymLT.ToString() ||
                token.Value == Tokenizer.SymGT.ToString() ||
                token.Value == Tokenizer.SymEq.ToString());
        }


        private static bool IsReturnType(Token token)
        {
            return (IsVarType(token) || 
                (token.Type == TokenTypes.Keyword &&
                token.Value == Tokenizer.Void));
        }


        private static bool IsRightBracket(Token token)
        {
            return token.Type == TokenTypes.Symbol && token.Value == Tokenizer.SymRightBracket.ToString();
        }


        private static bool IsRightCurly(Token token)
        {
            return token.Type == TokenTypes.Symbol && token.Value == Tokenizer.SymRightCurly.ToString();
        }


        private static bool IsRightParen(Token token)
        {
            return token.Type == TokenTypes.Symbol && token.Value == Tokenizer.SymRightParen.ToString();
        }


        private static bool IsSemiColon(Token token)
        {
            return token.Type == TokenTypes.Symbol && token.Value == Tokenizer.SymSemiColon.ToString();
        }


        private static bool IsSubroutineKeyword(Token token)
        {
            return (token.Type == TokenTypes.Keyword &&
                (token.Value == Tokenizer.Ctor ||
                token.Value == Tokenizer.Function ||
                token.Value == Tokenizer.Method));
        }


        private static bool IsUnaryOp(Token token)
        {
            return token.Type == TokenTypes.Symbol &&
                (token.Value == Tokenizer.SymMinus.ToString() ||
                token.Value == Tokenizer.SymNot.ToString());
        }


        private static bool IsVarType(Token token)
        {
            return IsBuiltinType(token) || token.Type == TokenTypes.Identifier;
        }


        public static Class Parse(IList<Token> tokens)
        {
            Class parseTree = ParseClass(tokens);
            return parseTree;
        }


        private static Class ParseClass(IList<Token> tokens)
        {
            if (tokens[0].Type == TokenTypes.Keyword &&
                tokens[0].Value == Tokenizer.Class &&
                tokens[1].Type == TokenTypes.Identifier &&
                tokens[2].Type == TokenTypes.Symbol &&
                tokens[2].Value == Tokenizer.SymLeftCurly.ToString())
            {
                var classNode = new Class(tokens[1]);

                int index = 3;
                ParseClassVarDecs(tokens, classNode, ref index);
                ParseSubroutines(tokens, classNode, ref index);

                if (!IsRightCurly(tokens[index]))
                    throw new InvalidOperationException("Expected right curly brace at end of class: " + tokens[index].Value);

                return classNode;
            }
            else
                throw new InvalidOperationException("Failed to parse class declaration.");
        }


        private static void ParseClassVarDecs(IList<Token> tokens, Node parent, ref int index)
        {
            Token staticOrFieldToken = tokens[index];
            while (staticOrFieldToken.Type == TokenTypes.Keyword &&
                (staticOrFieldToken.Value == Tokenizer.Static || staticOrFieldToken.Value == Tokenizer.Field))
            {
                Token typeToken = tokens[++index];
                if (!IsVarType(typeToken))
                    throw new InvalidOperationException("Type expected: " + typeToken.Value);

                bool done = false;
                while (!done)
                {
                    Token nameToken = tokens[++index];
                    if (nameToken.Type != TokenTypes.Identifier)
                        throw new InvalidOperationException("Variable name expected: " + nameToken.Value);

                    var classVarDecNode = new ClassVarDec(staticOrFieldToken, typeToken, nameToken);
                    parent.Children.Add(classVarDecNode);

                    if (!IsComma(tokens[++index]))
                        done = true;
                }

                if (!IsSemiColon(tokens[index]))
                    throw new InvalidOperationException("Expected semicolon: " + tokens[index].Value);

                staticOrFieldToken = tokens[++index];
            }
        }


        private static void ParseDoStatement(IList<Token> tokens, Node parent, ref int index)
        {
            SubroutineCall subCall = ParseSubroutineCall(tokens, ref index);

            if (!IsSemiColon(tokens[index]))
                throw new InvalidOperationException("Expected symbol ';' in do statement: " + tokens[index].Value);

            var doStatement = new DoStatement(subCall);
            parent.Children.Add(doStatement);

            index++;
        }


        private static Expression ParseExpression(IList<Token> tokens, ref int index)
        {
            Terminal term = ParseTerminal(tokens, ref index);
            if (term == null)
                return null;

            Token op = tokens[index];
            if (IsOp(op))
            {
                index++;
                Terminal term2 = ParseTerminal(tokens, ref index);
                return new Expression(term, op, term2);
            }
            else
                return new Expression(term);
        }


        private static IList<Expression> ParseExpressionList(IList<Token> tokens, ref int index)
        {
            var expList = new List<Expression>();
            Expression expression = ParseExpression(tokens, ref index);
            if (expression != null)
            {
                expList.Add(expression);

                while (IsComma(tokens[index]))
                {
                    index++;
                    Expression exp = ParseExpression(tokens, ref index);
                    if (exp != null)
                        expList.Add(exp);
                    else
                        break;
                }
            }

            return expList;
        }


        private static void ParseIfStatement(IList<Token> tokens, Node parent, ref int index)
        {
            if (!IsLeftParen(tokens[index]))
                throw new InvalidOperationException("Expected symbol '(' in if statement: " + tokens[index].Value);

            index++;
            Expression condition = ParseExpression(tokens, ref index);

            if (!IsRightParen(tokens[index]))
                throw new InvalidOperationException("Expected symbol ')' in if statement: " + tokens[index].Value);

            if (!IsLeftCurly(tokens[++index]))
                throw new InvalidOperationException("Expected symbol '{' in if statement body: " + tokens[index].Value);
            
            index++;
            Statements ifStatements = ParseStatements(tokens, ref index);

            if (!IsRightCurly(tokens[index]))
                throw new InvalidOperationException("Expected symbol '}' in if statement body: " + tokens[index].Value);

            Statements elseStatements = null;
            var elseToken = tokens[++index];
            if (IsElse(elseToken))
            {
                if (!IsLeftCurly(tokens[++index]))
                    throw new InvalidOperationException("Expected symbol '{' in else statement body: " + tokens[index].Value);

                index++;
                elseStatements = ParseStatements(tokens, ref index);

                if (!IsRightCurly(tokens[index]))
                    throw new InvalidOperationException("Expected symbol '}' in else statement body: " + tokens[index].Value);

                index++;
            }
            else
                elseStatements = new Statements();

            var ifNode = new IfStatement(condition, ifStatements, elseStatements);
            parent.Children.Add(ifNode);
        }


        private static void ParseLetStatement(IList<Token> tokens, Node parent, ref int index)
        {
            Token varName = tokens[index];
            if (varName.Type != TokenTypes.Identifier)
                throw new InvalidOperationException("Expected variable name in let statment: " + varName.Value);

            Token symbol = tokens[++index];
            if (symbol.Type != TokenTypes.Symbol)
                throw new InvalidOperationException("Expected symbol '[' or '=': " + symbol.Value);

            Expression indexer = null;
            if (IsLeftBracket(symbol))
            {
                index++;
                indexer = ParseExpression(tokens, ref index);

                if (!IsRightBracket(tokens[index]))
                    throw new InvalidOperationException("Expected symbol ']': " + tokens[index].Value);

                symbol = tokens[++index];
            }

            if (symbol.Value != Tokenizer.SymEq.ToString())
                throw new InvalidOperationException("Expected symbol '=': " + symbol.Value);

            index++;
            Expression assignment = ParseExpression(tokens, ref index);

            if (!IsSemiColon(tokens[index]))
                throw new InvalidOperationException("Expected ';' in let statment: " + tokens[index].Value);

            var letStatement = new LetStatement(varName, assignment, indexer);
            parent.Children.Add(letStatement);

            index++;
        }


        private static void ParseReturnStatement(IList<Token> tokens, Node parent, ref int index)
        {
            Expression retExpression = null;
            if (!IsSemiColon(tokens[index]))
                retExpression = ParseExpression(tokens, ref index);

            if (!IsSemiColon(tokens[index]))
                throw new InvalidOperationException("Expected symbol ';' in return statement: " + tokens[index].Value);

            var retNode = new ReturnStatement(retExpression);
            parent.Children.Add(retNode);

            index++;
        }


        private static Statements ParseStatements(IList<Token> tokens, ref int index)
        {
            var statements = new Statements();

            var keyword = tokens[index];
            while (keyword.Type == TokenTypes.Keyword)
            {
                index++;
                switch (keyword.Value)
                {
                    case Tokenizer.Let:
                        ParseLetStatement(tokens, statements, ref index);
                        break;
                    case Tokenizer.If:
                        ParseIfStatement(tokens, statements, ref index);
                        break;
                    case Tokenizer.While:
                        ParseWhileStatement(tokens, statements, ref index);
                        break;
                    case Tokenizer.Do:
                        ParseDoStatement(tokens, statements, ref index);
                        break;
                    case Tokenizer.Return:
                        ParseReturnStatement(tokens, statements, ref index);
                        break;
                    default:
                        throw new InvalidOperationException("Expected a valid starting statment keyword: " + tokens[index].Value);
                }

                keyword = tokens[index];
            }

            return statements;
        }


        private static void ParseSubroutineBody(IList<Token> tokens, Subroutine parent, ref int index)
        {
            if (!IsLeftCurly(tokens[index]))
                throw new InvalidOperationException("Expected '{' to start subroutine body: " + tokens[index].Value);

            index++;
            while (ParseVarDec(tokens, parent, ref index));

            Statements statements = ParseStatements(tokens, ref index);
            parent.Children.Add(statements);

            if (!IsRightCurly(tokens[index]))
                throw new InvalidOperationException("Expected '}' to end subroutine body: " + tokens[index].Value);

            index++;
        }


        private static SubroutineDeclaration ParseSubroutineDec(IList<Token> tokens, ref int index)
        {
            Token subroutineTypeToken = tokens[index];
            if (IsSubroutineKeyword(subroutineTypeToken))
            {
                Token retTypeToken = tokens[++index];
                if (!IsReturnType(retTypeToken))
                    throw new InvalidOperationException("Invalid return type: " + retTypeToken.Value);

                Token routineName = tokens[++index];
                if (routineName.Type != TokenTypes.Identifier)
                    throw new InvalidOperationException("Subroutine name expected: " + routineName.Value);

                if (!IsLeftParen(tokens[++index]))
                    throw new InvalidOperationException("Expected left paren for subroutine parameter list: " + tokens[index].Value);

                var paramList = new Dictionary<Token, Token>();
                bool done = false;
                while (!done)
                {
                    Token paramType = tokens[++index];
                    if (IsVarType(paramType))
                    {
                        Token paramName = tokens[++index];
                        if (paramName.Type != TokenTypes.Identifier)
                            throw new InvalidOperationException("Expected parameter name: " + paramName.Value);

                        paramList.Add(paramType, paramName);

                        if (!IsComma(tokens[++index]))
                            done = true;
                    }
                    else
                    {
                        done = true;
                        //index++;
                    }
                }

                if (!IsRightParen(tokens[index]))
                    throw new InvalidOperationException("Expected right paren for subroutine parameter list: " + tokens[index].Value);

                index++;
                var subroutineDeclaration = new SubroutineDeclaration(subroutineTypeToken, retTypeToken, routineName, paramList);
                return subroutineDeclaration;
            }
            else
                return null;
        }


        private static SubroutineCall ParseSubroutineCall(IList<Token> tokens, ref int index)
        {
            Token name = tokens[index];
            if (name.Type != TokenTypes.Identifier)
                throw new InvalidOperationException("Expected a name identifier in do statement: " + name.Value);

            Token symbol = tokens[++index];
            if (symbol.Type != TokenTypes.Symbol)
                throw new InvalidOperationException("Expected symbol '(' or '.' in do statement: " + symbol.Value);

            if (IsLeftParen(symbol))
            {
                IList<Expression> expList = null;
                if (!IsRightParen(tokens[++index]))
                {
                    expList = ParseExpressionList(tokens, ref index);

                    if (!IsRightParen(tokens[index]))
                        throw new InvalidOperationException("Expected symbol ')' in do statement: " + tokens[index].Value);
                }

                index++;
                return new SubroutineCall(name, expList ?? new Expression[0]);
            }
            else
            {
                symbol = tokens[index];
                if (!IsDot(symbol))
                    throw new InvalidOperationException("Expected symbol '.' in do statement: " + symbol.Value);

                Token subroutineName = tokens[++index];
                if (subroutineName.Type != TokenTypes.Identifier)
                    throw new InvalidOperationException("Expected subroutine identifier in do statement: " + subroutineName.Value);

                if (!IsLeftParen(tokens[++index]))
                    throw new InvalidOperationException("Expected symbol '(' in do statement: " + tokens[index].Value);

                IList<Expression> expList = null;
                if (!IsRightParen(tokens[++index]))
                {
                    expList = ParseExpressionList(tokens, ref index);

                    if (!IsRightParen(tokens[index]))
                        throw new InvalidOperationException("Expected symbol ')' in do statement: " + tokens[index].Value);
                }

                index++;
                return new SubroutineCall(name, subroutineName, expList ?? new Expression[0]);
            }
        }


        private static void ParseSubroutines(IList<Token> tokens, Node parent, ref int index)
        {
            bool done = false;
            while (!done)
            {
                SubroutineDeclaration subDec = ParseSubroutineDec(tokens, ref index);
                if (subDec != null)
                {
                    var subNode = new Subroutine(subDec);
                    ParseSubroutineBody(tokens, subNode, ref index);
                    parent.Children.Add(subNode);
                }
                else
                    done = true;
            }
        }


        private static Terminal ParseTerminal(IList<Token> tokens, ref int index)
        {
            Terminal termNode = null;

            Token term1Token = tokens[index];
            if (IsConstantValue(term1Token) || IsKeywordConstant(term1Token))
            {
                termNode = new Terminal(term1Token);
                index++;
            }
            else if (term1Token.Type == TokenTypes.Identifier)
            {
                Token symbol = tokens[++index];
                if (IsLeftBracket(symbol))
                {
                    index++;
                    Expression exp = ParseExpression(tokens, ref index);

                    if (!IsRightBracket(tokens[index]))
                        throw new InvalidOperationException("Expected ']' in terminal: " + tokens[index].Value);

                    termNode = new Terminal(term1Token, exp);
                    index++;
                }
                else if (IsDot(symbol))
                {
                    index--;
                    SubroutineCall subCall = ParseSubroutineCall(tokens, ref index);
                    termNode = new Terminal(subCall);
                }
                else
                    termNode = new Terminal(term1Token);
            }
            else if (IsUnaryOp(term1Token))
            {
                index++;
                Terminal term = ParseTerminal(tokens, ref index);
                termNode = new Terminal(term1Token, term);
            }
            else if (IsLeftParen(term1Token))
            {
                index++;
                Expression exp = ParseExpression(tokens, ref index);

                if (!IsRightParen(tokens[index]))
                    throw new InvalidOperationException("Expected ')' in terminal: " + tokens[index].Value);

                termNode = new Terminal(exp);
                index++;
            }
            else
            {
                SubroutineCall subCall = ParseSubroutineCall(tokens, ref index);
                termNode = new Terminal(subCall);
            }

            return termNode;
        }


        private static bool ParseVarDec(IList<Token> tokens, Node parent, ref int index)
        {
            Token varToken = tokens[index];
            if (varToken.Type != TokenTypes.Keyword || varToken.Value != Tokenizer.Var)
                return false;

            Token typeToken = tokens[++index];
            if (!IsVarType(typeToken))
                throw new InvalidOperationException("Invalid variable type: " + typeToken.Value);

            bool done = false;
            while (!done)
            {
                Token varNameToken = tokens[++index];
                if (varNameToken.Type != TokenTypes.Identifier)
                    throw new InvalidOperationException("Invalid variable name: " + varNameToken.Value);

                var varDecNode = new SubroutineVarDec(typeToken, varNameToken);
                parent.Children.Add(varDecNode);

                if (!IsComma(tokens[++index]))
                    done = true;
            }

            if (!IsSemiColon(tokens[index]))
                throw new InvalidOperationException("Expected semicolon: " + tokens[index].Value);

            index++;
            return true;
        }


        private static void ParseWhileStatement(IList<Token> tokens, Node parent, ref int index)
        {
            if (!IsLeftParen(tokens[index]))
                throw new InvalidOperationException("Expected symbol '(' in while condition: " + tokens[index].Value);

            index++;
            Expression condition = ParseExpression(tokens, ref index);

            if (!IsRightParen(tokens[index]))
                throw new InvalidOperationException("Expected symbol ')' in while condition: " + tokens[index].Value);

            if (!IsLeftCurly(tokens[++index]))
                throw new InvalidOperationException("Expected symbol '{' in while body: " + tokens[index].Value);

            index++;
            Statements statements = ParseStatements(tokens, ref index);

            if (!IsRightCurly(tokens[index]))
                throw new InvalidOperationException("Expected symbol '}' in while body: " + tokens[index].Value);

            var whileStatement = new WhileStatement(condition, statements);
            parent.Children.Add(whileStatement);

            index++;
        }
    }
}
