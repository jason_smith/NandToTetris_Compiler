﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JackCompiler.Parsing
{
    class DoStatement : Node
    {
        readonly SubroutineCall _subCall;

        public DoStatement(SubroutineCall subCall)
        {
            _subCall = subCall;
        }

        public SubroutineCall SubroutineCall
        {
            get { return _subCall; }
        }

        public override string ToString()
        {
            return "Do: " + _subCall.ToString();
        }
    }
}
