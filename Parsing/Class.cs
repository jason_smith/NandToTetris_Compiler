﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using JackCompiler.Tokenizing;

namespace JackCompiler.Parsing
{
    class Class : Node
    {
        readonly Token _name;

        public Class(Token name)
        {
            _name = name;
        }

        public string Name
        {
            get { return _name.Value; }
        }

        public override string ToString()
        {
            return "Class: Name=" + _name;
        }
    }
}
