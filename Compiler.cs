﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using JackCompiler.Parsing;
using JackCompiler.Tokenizing;

namespace JackCompiler
{
    class Compiler
    {
        #region Fields

        const string Ext = ".vm";
        readonly Dictionary<string, Subroutine> _subroutines;
        readonly SymbolTable _classSymTable;
        readonly VmWriter _vmWriter;
        readonly Class _class;
        readonly string _inputFile;
        int _ifCount;
        int _whileCount;

        #endregion

        #region Constructors

        public Compiler(Class classNode, string inputFile)
        {
            _class = classNode;
            _inputFile = inputFile;
            _vmWriter = new VmWriter();
            _subroutines = new Dictionary<string, Subroutine>();
            _classSymTable = GetSymbolTable(classNode.Children.OfType<ClassVarDec>());

            foreach (Subroutine subroutine in _class.Children.OfType<Subroutine>())
                _subroutines.Add(subroutine.Declaration.Name, subroutine);
        }

        #endregion

        #region Methods

        public void Compile()
        {
            foreach (Subroutine subroutine in _subroutines.Values)
                CompileSubroutine(subroutine);

            this.WriteFile();
        }

        private void CompileArithmetic(char op)
        {
            switch (op)
            {
                case Tokenizer.SymAnd:
                    _vmWriter.WriteArithmetic("and");
                    break;
                case Tokenizer.SymDiv:
                    _vmWriter.WriteCall("Math.divide", 2);
                    break;
                case Tokenizer.SymGT:
                    _vmWriter.WriteArithmetic("gt");
                    break;
                case Tokenizer.SymLT:
                    _vmWriter.WriteArithmetic("lt");
                    break;
                case Tokenizer.SymMinus:
                    _vmWriter.WriteArithmetic("sub");
                    break;
                case Tokenizer.SymMult:
                    _vmWriter.WriteCall("Math.multiply", 2);
                    break;
                case Tokenizer.SymOr:
                    _vmWriter.WriteArithmetic("or");
                    break;
                case Tokenizer.SymPlus:
                    _vmWriter.WriteArithmetic("add");
                    break;
                case Tokenizer.SymEq:
                    _vmWriter.WriteArithmetic("eq");
                    break;
                case Tokenizer.SymNot:
                    _vmWriter.WriteArithmetic("not");
                    break;
                default:
                    throw new InvalidOperationException("Invalid operator: " + op);
            }
        }

        private void CompileDoStatement(DoStatement statement, SymbolTable subSymTable)
        {
            this.CompileSubroutineCall(statement.SubroutineCall, subSymTable);
            _vmWriter.WritePop(VmSegment.Temp, "0"); // pop return value onto temp 0
        }

        private void CompileExpression(Expression expression, SymbolTable subSymTable)
        {
            this.CompileTerm(expression.Term1, subSymTable);

            if (expression.Term2 != null)
            {
                this.CompileTerm(expression.Term2, subSymTable);
                this.CompileOperator(expression.Operator);
            }
        }

        private void CompileIfStatement(IfStatement statement, SymbolTable subSymTable)
        {
            string ifTrue = "ifTrue_" + _ifCount.ToString();
            string endIf = "endIf_" + _ifCount.ToString();
            _ifCount++;

            this.CompileExpression(statement.Condition, subSymTable);
            _vmWriter.WriteIf(ifTrue);

            this.CompileStatements(statement.ElseStatements, subSymTable);
            _vmWriter.WriteGoto(endIf);

            _vmWriter.WriteLabel(ifTrue);
            this.CompileStatements(statement.IfStatements, subSymTable);

            _vmWriter.WriteLabel(endIf);
        }

        private void CompileLetStatement(LetStatement statement, SymbolTable subSymTable)
        {
            this.CompileExpression(statement.Assignment, subSymTable);

            SymbolTable.Info info = this.GetInfo(statement.VarName, subSymTable);
            VmSegment seg = this.GetVmSegment(info);

            if (statement.Indexer != null)
            {
                this.CompileExpression(statement.Indexer, subSymTable);
                _vmWriter.WritePush(seg, info.Index);
                this.CompileArithmetic(Tokenizer.SymPlus);
                _vmWriter.WritePop(VmSegment.Pointer, "1");
                _vmWriter.WritePop(VmSegment.That, "0");
            }
            else
                _vmWriter.WritePop(seg, info.Index);
        }

        private void CompileOperator(Token op)
        {
            this.CompileArithmetic(op.Value[0]);
        }

        private void CompileReturnStatement(ReturnStatement statement, SymbolTable subSymTable)
        {
            if (statement.ReturnExpression != null)
                this.CompileExpression(statement.ReturnExpression, subSymTable);
            else
                _vmWriter.WritePush(VmSegment.Const, "0");

            _vmWriter.WriteReturn();
        }

        private void CompileStatements(Statements statements, SymbolTable subSymTable)
        {
            foreach (Node statement in statements.Children)
            {
                var letStatement = statement as LetStatement;
                if (letStatement != null)
                {
                    this.CompileLetStatement(letStatement, subSymTable);
                    continue;
                }

                var doStatement = statement as DoStatement;
                if (doStatement != null)
                {
                    this.CompileDoStatement(doStatement, subSymTable);
                    continue;
                }

                var ifStatement = statement as IfStatement;
                if (ifStatement != null)
                {
                    this.CompileIfStatement(ifStatement, subSymTable);
                    continue;
                }

                var whileStatement = statement as WhileStatement;
                if (whileStatement != null)
                {
                    this.CompileWhileStatement(whileStatement, subSymTable);
                    continue;
                }

                var returnStatement = statement as ReturnStatement;
                if (returnStatement == null)
                    throw new InvalidOperationException("Unknown statement encountered.");

                this.CompileReturnStatement(returnStatement, subSymTable);
            }
        }

        private void CompileString(string str)
        {
            _vmWriter.WritePush(VmSegment.Const, str.Length.ToString());
            _vmWriter.WriteCall("String.new", 1);

            for (int i = 0; i < str.Length; i++)
            {
                _vmWriter.WritePush(VmSegment.Const, ((int)str[i]).ToString());
                _vmWriter.WriteCall("String.appendChar", 2);
            }
        }

        private void CompileSubroutine(Subroutine sub)
        {
            SymbolTable subSymTable = this.GetSymbolTable(sub.Declaration, sub.Children.OfType<SubroutineVarDec>());

            int nLocals = sub.Children.OfType<SubroutineVarDec>().Count();
            _vmWriter.WriteFunction(_class.Name + "." + sub.Declaration.Name, nLocals);

            if (sub.Declaration.IsMethod)
            {
                // set 'this' segment
                _vmWriter.WritePush(VmSegment.Arg, "0");
                _vmWriter.WritePop(VmSegment.Pointer, "0");
            }
            else if (sub.Declaration.IsCtor)
            {
                int numFields = _classSymTable.GetInfos(i => i.Kind == Kinds.Field).Count;
                _vmWriter.WritePush(VmSegment.Const, numFields.ToString()); // push the number of 16-bit words needed for the class
                _vmWriter.WriteCall("Memory.alloc", 1);
                _vmWriter.WritePop(VmSegment.Pointer, "0"); // anchors 'this' at the base address
            }

            var statements = sub.Children.OfType<Statements>().FirstOrDefault();
            if (statements != null)
                this.CompileStatements(statements, subSymTable);
        }

        private void CompileSubroutineCall(SubroutineCall call, SymbolTable subSymTable)
        {
            string fullSubName = string.Empty;
            int paramCount = 0;

            if (call.VariableName != null)
            {
                // if the variableName is found in a symbol table, the subroutine is a method
                SymbolTable.Info info = this.GetInfo(call.VariableName, subSymTable);
                if (info != null)
                {
                    // push object on stack as first argument
                    VmSegment seg = this.GetVmSegment(info);
                    _vmWriter.WritePush(seg, info.Index);
                    paramCount++;
                    fullSubName = info.Type + ".";
                }
                else
                    fullSubName = call.VariableName + ".";
            }
            else if (this.IsLocalMethod(call.SubroutineName))
            {
                _vmWriter.WritePush(VmSegment.Pointer, "0");
                paramCount++;
                fullSubName = _class.Name + ".";
            }
            else
                fullSubName = _class.Name + ".";

            foreach (Expression param in call.ParamList)
                this.CompileExpression(param, subSymTable);

            fullSubName += call.SubroutineName;
            paramCount += call.ParamList.Count;
            _vmWriter.WriteCall(fullSubName, paramCount);
        }

        private void CompileTerm(Terminal term, SymbolTable subSymTable)
        {
            if (term.IsConstValue)
            {
                if (term.Token.Type == TokenTypes.Integer)
                    _vmWriter.WritePush(VmSegment.Const, term.Token.Value);
                else if (term.Token.Type == TokenTypes.String)
                    CompileString(term.Token.Value);
                else
                    throw new InvalidOperationException("Invalid constant type: " + term.Token.Type);
            }
            else if (term.IsVarName)
            {
                SymbolTable.Info info = this.GetInfo(term.Token.Value, subSymTable);
                VmSegment seg = this.GetVmSegment(info);
                _vmWriter.WritePush(seg, info.Index);
            }
            else if (term.IsUnaryOpTerm)
            {
                this.CompileTerm(term.Term, subSymTable);

                if (term.Token.Value[0] == Tokenizer.SymNot)
                    _vmWriter.WriteArithmetic("not");
                else if (term.Token.Value[0] == Tokenizer.SymMinus)
                    _vmWriter.WriteArithmetic("neg");
                else
                    throw new InvalidOperationException("Invalid unary op term: " + term.Token.Value);
            }
            else if (term.IsArray)
            {
                this.CompileExpression(term.Expression, subSymTable);

                SymbolTable.Info info = GetInfo(term.Token.Value, subSymTable);
                VmSegment seg = this.GetVmSegment(info);

                _vmWriter.WritePush(seg, info.Index);
                CompileArithmetic(Tokenizer.SymPlus);

                _vmWriter.WritePop(VmSegment.Pointer, "1");
                _vmWriter.WritePush(VmSegment.That, "0");
            }
            else if (term.IsExpression)
                this.CompileExpression(term.Expression, subSymTable);
            else if (term.IsSubroutineCall)
            {
                this.CompileSubroutineCall(term.SubroutineCall, subSymTable);
            }
            else if (term.IsKeywordConstant)
            {
                switch (term.Token.Value)
                {
                    case Tokenizer.This:
                        _vmWriter.WritePush(VmSegment.Pointer, "0");
                        break;
                    case Tokenizer.True:
                        _vmWriter.WritePush(VmSegment.Const, "0");
                        CompileArithmetic(Tokenizer.SymNot);
                        break;
                    case Tokenizer.False:
                    case Tokenizer.Null:
                        _vmWriter.WritePush(VmSegment.Const, "0");
                        break;
                }
            }
        }

        private void CompileWhileStatement(WhileStatement statement, SymbolTable subSymTable)
        {
            string startLabel = "startWhile_" + _whileCount.ToString();
            string endLabel = "endWhile_" + _whileCount.ToString();
            _whileCount++;

            _vmWriter.WriteLabel(startLabel);
            this.CompileExpression(statement.Condition, subSymTable);

            // negate condition
            this.CompileArithmetic(Tokenizer.SymNot);
            _vmWriter.WriteIf(endLabel);

            Statements statements = statement.Children.OfType<Statements>().FirstOrDefault();
            if (statements != null)
                this.CompileStatements(statements, subSymTable);

            _vmWriter.WriteGoto(startLabel);
            _vmWriter.WriteLabel(endLabel);
        }

        private SymbolTable.Info GetInfo(string name, SymbolTable subSymTable)
        {
            SymbolTable.Info info = subSymTable.GetInfo(name);
            if (info == null)
                info = _classSymTable.GetInfo(name);

            return info;
        }

        private string GetKind(ClassVarDec varDec)
        {
            if (varDec.IsField)
                return Kinds.Field;

            if (varDec.IsStatic)
                return Kinds.Static;

            throw new InvalidOperationException("Expected variable kind to be field or static.");
        }

        private SymbolTable GetSymbolTable(IEnumerable<ClassVarDec> varDecs)
        {
            var symTable = new SymbolTable();
            
            foreach (ClassVarDec varDec in varDecs)
            {
                string kind = this.GetKind(varDec);
                symTable.Add(varDec.VarName, varDec.TypeName, kind);
            }

            return symTable;
        }

        private SymbolTable GetSymbolTable(SubroutineDeclaration subDec, IEnumerable<SubroutineVarDec> varDecs)
        {
            var symTable = new SymbolTable();
            
            if (subDec.IsMethod)
                symTable.Add("this", _class.Name, Kinds.Argument);

            foreach (KeyValuePair<Token, Token> pair in subDec.ParamList)
                symTable.Add(pair.Value.Value, pair.Key.Value, Kinds.Argument);
            
            foreach (SubroutineVarDec varDec in varDecs)
                symTable.Add(varDec.Name, varDec.Type, Kinds.Local);

            return symTable;
        }

        private VmSegment GetVmSegment(SymbolTable.Info info)
        {
            switch (info.Kind)
            {
                case Kinds.Argument: return VmSegment.Arg;
                case Kinds.Field: return VmSegment.This;
                case Kinds.Local: return VmSegment.Local;
                case Kinds.Static: return VmSegment.Static;
                default: throw new InvalidOperationException("Failed to map 'kind' to 'VmSegment': " + info.Kind);
            }
        }

        private bool IsLocalMethod(string subroutineName)
        {
            Subroutine sub;
            bool found = _subroutines.TryGetValue(subroutineName, out sub);
            return (found && sub.Declaration.IsMethod);
        }

        private void WriteFile()
        {
            string fileName = Path.GetFileNameWithoutExtension(_inputFile) + Ext;
            string outFile = Path.Combine(Path.GetDirectoryName(_inputFile), fileName);

            File.WriteAllText(outFile, _vmWriter.ToString());
        }

        #endregion
    }
}
